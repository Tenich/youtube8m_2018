cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=1 python inference.py --input_model_tgz ${ROOT_DIR}/models/prelu_wide_v1.tgz  \
                                           --output_file=${ROOT_DIR}/preli_wide_v1.csv \
                                           --input_data_pattern=${ROOT_DIR}/video/test*.tfrecord \
                                           --num_readers=8 

