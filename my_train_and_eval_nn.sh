cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=0,1 python train.py \
                          --feature_names='mean_rgb,mean_audio' \
                          --feature_sizes='1024,128' \
                          --model="DenseModel" \
                          --train_data_pattern=${ROOT_DIR}/video/train*.tfrecord \
                          --train_dir ${ROOT_DIR}/models/dense \
                          --num_epochs 1000 \
                          --batch_size 2048 \
                          --base_learning_rate 0.001 \
                          --num_readers=32 \
                          --num_gpu=2 \
                          --start_new_model \
                          --export_model_steps=1000 #\
#                          --use_class_weights


CUDA_VISIBLE_DEVICES=0 python eval.py --eval_data_pattern=${ROOT_DIR}/video/validate*.tfrecord \
                                      --train_dir ${ROOT_DIR}/models/dense \
                                      --run_once \
                                      --batch_size=2048 \
                                      --:num_readers=16
