cd starterkit

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=0 python train.py \
                          --feature_names='mean_rgb,mean_audio' \
                          --feature_sizes='1024,128' \
                          --model="NNModel" \
                          --train_data_pattern=${ROOT_DIR}/video/train*.tfrecord \
                          --train_dir ${ROOT_DIR}/models/v0 \
                          --start_new_model \
                          --num_epochs=10 \
                          --batch_size=4096 \
                          --num_readers=16 \
                          --num_gpu=1

#CUDA_VISIBLE_DEVICES=0 python eval.py --eval_data_pattern=${ROOT_DIR}/video/validate*.tfrecord \
#                                      --train_dir ${ROOT_DIR}/models/v0 \
#                                      --run_once \
#                                      --batch_size=8192 \
#                                      --num_readers=16

#CUDA_VISIBLE_DEVICES=0 python inference.py --train_dir ${ROOT_DIR}/models/v0  \
#                                           --output_file=${ROOT_DIR}/kaggle_solution.csv \
#                                           --input_data_pattern=${ROOT_DIR}/video/test*.tfrecord
#
