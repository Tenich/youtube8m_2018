cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=0 python train.py \
                          --feature_names='mean_rgb,mean_audio' \
                          --feature_sizes='1024,128' \
                          --model="RELUWideModel" \
                          --train_data_pattern=${ROOT_DIR}/video/train*.tfrecord \
                          --train_dir ${ROOT_DIR}/models/relu_wide \
                          --num_epochs 1000 \
                          --batch_size 1024 \
                          --base_learning_rate 0.001 \
                          --num_readers=8 \
                          --num_gpu=1 \
                          --export_model_steps=5000 \
                          --start_new_model \
                          --use_surrogate_loss
#                          --use_class_weights


CUDA_VISIBLE_DEVICES=0 python eval.py --eval_data_pattern=${ROOT_DIR}/video/validate*.tfrecord \
                                      --train_dir ${ROOT_DIR}/models/relu_wide \
                                      --run_once \
                                      --batch_size=1024 \
                                      --num_readers=8
