cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=1 python train.py \
                          --feature_names='mean_rgb,mean_audio' \
                          --feature_sizes='1024,128' \
                          --model="PRELUWiderModel" \
                          --train_data_pattern=${ROOT_DIR}/video/train*.tfrecord \
                          --train_dir ${ROOT_DIR}/models/prelu_wider \
                          --num_epochs 1000 \
                          --batch_size 1024 \
                          --base_learning_rate 0.0001 \
                          --learning_rate_decay=1.0 \
                          --learning_rate_decay_examples=10000000 \
                          --num_readers=8 \
                          --num_gpu=1 \
                          --export_model_steps=1000 \
                          --start_new_model 


CUDA_VISIBLE_DEVICES=1 python eval.py --eval_data_pattern=${ROOT_DIR}/video/validate*.tfrecord \
                                      --train_dir ${ROOT_DIR}/models/prelu_wider \
                                      --run_once \
                                      --batch_size=4096 \
                                      --num_readers=16
