cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

#CUDA_VISIBLE_DEVICES=0 python train.py \
#                          --feature_names='mean_rgb,mean_audio' \
#                          --feature_sizes='1024,128' \
#                          --model="PRELUWideModel" \
#                          --train_data_pattern=${ROOT_DIR}/video/*a*.tfrecord \
#                          --train_dir ${ROOT_DIR}/models/prelu_wide \
#                          --max_steps 2010 \
#                          --batch_size 1024 \
#                          --base_learning_rate 0.001 \
#                          --learning_rate_decay=1.0 \
#                          --learning_rate_decay_examples=10000000 \
#                          --num_readers=8 \
#                          --num_gpu=1 \
#                          --export_model_steps=100  \
#                          --start_new_model 

#CUDA_VISIBLE_DEVICES=0 python train.py \
#                          --feature_names='mean_rgb,mean_audio' \
#                          --feature_sizes='1024,128' \
#                          --model="PRELUWideModel" \
#                          --train_data_pattern=${ROOT_DIR}/video/*a*.tfrecord \
#                          --train_dir ${ROOT_DIR}/models/prelu_wide \
#                          --max_steps 10020 \
#                          --batch_size 1024 \
#                          --base_learning_rate 0.0001 \
#                          --learning_rate_decay=1.0 \
#                          --learning_rate_decay_examples=10000000 \
#                          --num_readers=8 \
#                          --num_gpu=1 \
#                          --export_model_steps=100  

#CUDA_VISIBLE_DEVICES=0 python train.py \
#                          --feature_names='mean_rgb,mean_audio' \
#                          --feature_sizes='1024,128' \
#                          --model="PRELUWideModel" \
#                          --train_data_pattern=${ROOT_DIR}/video/*a*.tfrecord \
#                          --train_dir ${ROOT_DIR}/models/prelu_wide \
#                          --num_epochs 100000 \
#                          --max_steps 1000000000 \
#                          --batch_size 1024 \
#                          --base_learning_rate 0.00001 \
#                          --learning_rate_decay=1.0 \
#                          --learning_rate_decay_examples=10000000 \
#                          --num_readers=8 \
#                          --num_gpu=1 \
#                          --export_model_steps=5000 


CUDA_VISIBLE_DEVICES= python eval.py --eval_data_pattern=${ROOT_DIR}/video/validate*.tfrecord \
                                      --train_dir ${ROOT_DIR}/models/prelu_wide \
                                      --run_once \
                                      --batch_size=4096 \
                                      --num_readers=16
