# Copyright 2016 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS-IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Contains model definitions."""
import math

import models
import tensorflow as tf
import utils

from tensorflow import flags

FLAGS = flags.FLAGS
flags.DEFINE_integer(
    "moe_num_mixtures", 2,
    "The number of mixtures (excluding the dummy 'expert') used for MoeModel.")

class LogisticModel(models.BaseModel):
  """Logistic model with L2 regularization."""

  def create_model(self, model_input, vocab_size, is_training=True, l2_penalty=1e-8, **unused_params):
    """Creates a logistic model.

    Args:
      model_input: 'batch' x 'num_features' matrix of input features.
      vocab_size: The number of classes in the dataset.

    Returns:
      A dictionary with a tensor containing the probability predictions of the
      model in the 'predictions' key. The dimensions of the tensor are
      batch_size x num_classes."""
    model_input = tf.layers.batch_normalization(model_input, training=is_training)
    output = tf.layers.dense(
        model_input, vocab_size, activation=None,
        kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_penalty))
    return {"predictions": output}

class MoeModel(models.BaseModel):
  """A softmax over a mixture of logistic models (with L2 regularization)."""

  def create_model(self,
                   model_input,
                   vocab_size,
                   num_mixtures=None,
                   l2_penalty=1e-8,
                   **unused_params):
    """Creates a Mixture of (Logistic) Experts model.

     The model consists of a per-class softmax distribution over a
     configurable number of logistic classifiers. One of the classifiers in the
     mixture is not trained, and always predicts 0.

    Args:
      model_input: 'batch_size' x 'num_features' matrix of input features.
      vocab_size: The number of classes in the dataset.
      num_mixtures: The number of mixtures (excluding a dummy 'expert' that
        always predicts the non-existence of an entity).
      l2_penalty: How much to penalize the squared magnitudes of parameter
        values.
    Returns:
      A dictionary with a tensor containing the probability predictions of the
      model in the 'predictions' key. The dimensions of the tensor are
      batch_size x num_classes.
    """
    num_mixtures = num_mixtures or FLAGS.moe_num_mixtures

    gate_activations = slim.fully_connected(
        model_input,
        vocab_size * (num_mixtures + 1),
        activation_fn=None,
        biases_initializer=None,
        weights_regularizer=slim.l2_regularizer(l2_penalty),
        scope="gates")
    expert_activations = slim.fully_connected(
        model_input,
        vocab_size * num_mixtures,
        activation_fn=None,
        weights_regularizer=slim.l2_regularizer(l2_penalty),
        scope="experts")

    gating_distribution = tf.nn.softmax(tf.reshape(
        gate_activations,
        [-1, num_mixtures + 1]))  # (Batch * #Labels) x (num_mixtures + 1)
    expert_distribution = tf.nn.sigmoid(tf.reshape(
        expert_activations,
        [-1, num_mixtures]))  # (Batch * #Labels) x num_mixtures

    final_probabilities_by_class_and_batch = tf.reduce_sum(
        gating_distribution[:, :num_mixtures] * expert_distribution, 1)
    final_probabilities = tf.reshape(final_probabilities_by_class_and_batch,
                                     [-1, vocab_size])
    return {"predictions": final_probabilities}


def small_block(input, is_training, units=1024 + 128, l2_scale=1e-7):
    dense = tf.layers.dense(input, units, activation=tf.nn.relu,
                            bias_initializer=tf.ones_initializer(),
                            kernel_initializer=tf.contrib.layers.variance_scaling_initializer(),
                            kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_scale))
    bn = tf.layers.batch_normalization(dense, training=is_training, epsilon=1e-7, fused=True)
    dp = tf.layers.dropout(bn, training=is_training)

    return dp

def large_block(input, is_training, n_blocks=3, units=1024 + 128):
    output = input
    for _ in range(n_blocks):
        output = small_block(output, is_training, units)
    return output


class NNModel(models.BaseModel):
    """NN with L2 regularization."""
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, l2_scale=1e-7, **unused_params):
        
        input = tf.layers.batch_normalization(model_input, scale=False, training=is_training)
        dense = small_block(input, is_training)

        for i in range(5):
            dense = large_block(dense, is_training) + dense
            dense = small_block(dense, is_training)
 
        logits = tf.layers.dense(dense, vocab_size, activation=None, 
                                 kernel_initializer=tf.contrib.layers.variance_scaling_initializer(), 
                                 kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_scale))

        return {"predictions": logits}

class SELUModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, **unused_params):
        #dense = tf.layers.batch_normalization(model_input, center=False, scale=False, training=is_training)
        dense = model_input 
        for i in range(10):
            dense = tf.layers.dense(dense, 2500, activation=tf.nn.selu,
                                    kernel_initializer=tf.keras.initializers.lecun_normal())
            if is_training:
                dense = tf.contrib.nn.alpha_dropout(dense, keep_prob=0.5)

        logits = tf.layers.dense(dense, vocab_size, activation=None,  
                                 kernel_initializer=tf.keras.initializers.lecun_normal())

        return {'predictions': logits}

class SELUWideModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, **unused_params):
        layers_size = [4096] * 5
        dense = tf.nn.l2_normalize(model_input, -1) 
        for i in range(5):
            dense = tf.layers.dense(dense, layers_size[i], activation=tf.nn.selu,
                                    kernel_initializer=tf.keras.initializers.lecun_normal())
            if is_training:
                dense = tf.contrib.nn.alpha_dropout(dense, keep_prob=0.9)
            else:
                dense = tf.contrib.nn.alpha_dropout(dense, keep_prob=1.0)

            #dense = tf.layers.dense(dense, 1024+128, activation=tf.nn.selu,
            #                        kernel_initializer=tf.keras.initializers.lecun_normal())

        logits = tf.layers.dense(dense, vocab_size, activation=None,  
                                 kernel_initializer=tf.keras.initializers.lecun_normal())

        return {'predictions': logits}

class DenseModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, **unused_params):
        prev = tf.nn.l2_normalize(model_input, -1) 

        #if is_training:
        #    prev += tf.random_normal(shape=tf.shape(prev), stddev=0.1)
        
        for i in range(1, 4):
            dense = tf.layers.dense(prev, i * (1024 + 128), activation=tf.nn.relu,
                                    kernel_initializer=tf.contrib.layers.variance_scaling_initializer())
            dense = tf.concat([prev, dense], axis=-1)

            prev = tf.layers.dropout(dense, rate=0.25, training=is_training)

        prev = tf.layers.dense(prev, 2 * vocab_size, activation=tf.nn.relu, 
                               kernel_initializer=tf.contrib.layers.variance_scaling_initializer())
        logits = tf.layers.dense(prev, vocab_size, activation=None,  
                                 kernel_initializer=tf.contrib.layers.variance_scaling_initializer())

        return {'predictions': logits}

def get_dense(input, units, activation=None, name=None):
    return tf.layers.dense(input, units, activation=activation, 
                           name=name, reuse=tf.AUTO_REUSE,
                           kernel_initializer=tf.contrib.layers.variance_scaling_initializer())

class RELUWideModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, **unused_params):
        layers_size = [7900] * 5

        dense = tf.nn.l2_normalize(model_input, -1) 
   
        for i in range(len(layers_size)):
            dense = tf.layers.dense(dense, layers_size[i], activation=tf.nn.relu,
                                    kernel_initializer=tf.contrib.layers.variance_scaling_initializer())
            
            dense = tf.layers.batch_normalization(dense, momentum=0.9, training=is_training, fused=True)
            dense = tf.layers.dropout(dense, rate=0.5, training=is_training)
        
        logits = tf.layers.dense(dense, vocab_size, activation=None,  
                                 kernel_initializer=tf.contrib.layers.variance_scaling_initializer())

        return {'predictions': logits}


class RELUDeepModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, **unused_params):
        layers_size = [4096] * 15

        dense = tf.nn.l2_normalize(model_input, -1) 
   
        for i in range(len(layers_size)):
            dense = tf.layers.dense(dense, layers_size[i], activation=tf.nn.relu,
                                    kernel_initializer=tf.contrib.layers.variance_scaling_initializer())
            
            dense = tf.layers.batch_normalization(dense, momentum=0.9, training=is_training, fused=True)
            dense = tf.layers.dropout(dense, rate=0.25, training=is_training)
        
        logits = tf.layers.dense(dense, vocab_size, activation=None,  
                                 kernel_initializer=tf.contrib.layers.variance_scaling_initializer())

        return {'predictions': logits}

class PRELUDeepModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, **unused_params):
        layers_size = [4096] * 15

        dense = tf.layers.batch_normalization(model_input, training=is_training) 
   
        for i in range(len(layers_size)):
            dense = tf.layers.dense(dense, layers_size[i], activation=tf.keras.layers.PReLU(),
                                    kernel_initializer=tf.contrib.layers.variance_scaling_initializer())
            
            dense = tf.layers.batch_normalization(dense, training=is_training)
            dense = tf.layers.dropout(dense, rate=0.1, training=is_training)
        
        logits = tf.layers.dense(dense, vocab_size, activation=None,  
                                 kernel_initializer=tf.contrib.layers.variance_scaling_initializer())

        return {'predictions': logits}

class PRELUWideModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, sample_dropout=False, **unused_params):
        layers_size = [7500] * 5

        # average over some iters while testing if we really want to 
        n_iters = 10 if (not is_training) and sample_dropout else 1
        
        # sample from dropout if we are training or really want to sample 
        activate_dropout = is_training or sample_dropout
        
        accumulator = 0
        for _ in range(n_iters):
            dense = tf.layers.batch_normalization(model_input, reuse=tf.AUTO_REUSE,
                                                  training=is_training, name='bn_input') 
   
            for i in range(len(layers_size)):
                dense = get_dense(dense, layers_size[i], activation=tf.keras.layers.PReLU(), name='dense_{}'.format(i))
                dense = tf.layers.batch_normalization(dense, reuse=tf.AUTO_REUSE, 
                                                      training=is_training, name='bn_{}'.format(i)) 
                dense = tf.layers.dropout(dense, rate=0.5, training=is_training)
        
            logits = get_dense(dense, vocab_size, activation=None, name='dense_final')
            accumulator += logits
        accumulator /= float(n_iters)

        return {'predictions': accumulator}


class PRELUWiderModel(models.BaseModel):
    def create_model(self, model_input, vocab_size, labels=None, is_training=True, **unused_params):
        layers_size = [8192] * 3

        dense = tf.layers.batch_normalization(model_input, training=is_training) 
   
        for i in range(len(layers_size)):
            dense = get_dense(dense, layers_size[i], activation=tf.keras.layers.PReLU())
            dense = tf.layers.batch_normalization(dense, training=is_training) 
            dense = tf.layers.dropout(dense, rate=0.5, training=is_training)
        
        logits = get_dense(dense, vocab_size, activation=None)
        return {'predictions': logits}
