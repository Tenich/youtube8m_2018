cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=0,1,2 python train.py \
                          --feature_names='rgb,audio' \
                          --feature_sizes='1024,128' \
                          --frame_features=True \
                          --model="LstmModel" \
                          --train_data_pattern=${ROOT_DIR}/frame/train*.tfrecord \
                          --train_dir ${ROOT_DIR}/models/lstm \
                          --num_epochs=10 \
                          --batch_size=100 \
                          --base_learning_rate=0.001 \
                          --num_readers=32 \
                          --num_gpu=3 \
                          --start_new_model \
                          --export_model_steps=50000


#CUDA_VISIBLE_DEVICES=1 python eval.py --eval_data_pattern=${ROOT_DIR}/frame/validate*.tfrecord \
#                                      --frame_features=True \
#                                      --feature_names='rgb,audio' \
#                                      --feature_sizes="1024,128" \
#                                      --train_dir ${ROOT_DIR}/models/lstm \
#                                      --run_once \
#                                      --batch_size=128 \
#                                      --num_readers=16
