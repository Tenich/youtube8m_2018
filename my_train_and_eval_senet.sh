cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=2,3 python train.py \
                          --feature_names='rgb,audio' \
                          --feature_sizes='1024,128' \
                          --frame_features=True \
                          --model="SENetModel" \
                          --train_data_pattern=${ROOT_DIR}/frame/train*.tfrecord \
                          --train_dir ${ROOT_DIR}/models/senet \
                          --num_epochs=10 \
                          --batch_size=64 \
                          --base_learning_rate=0.001 \
                          --num_readers=8 \
                          --num_gpu=2 \
                          --export_model_steps=1000 \
                          --start_new_model 


CUDA_VISIBLE_DEVICES=3 python eval.py --eval_data_pattern=${ROOT_DIR}/frame/validate*.tfrecord \
                                      --frame_features=True \
                                      --feature_names='rgb,audio' \
                                      --feature_sizes="1024,128" \
                                      --train_dir ${ROOT_DIR}/models/senet \
                                      --run_once \
                                      --batch_size=128 \
                                      --num_readers=32
