cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=0 python inference.py --input_model_tgz=${ROOT_DIR}/prelu_wide_v3.tgz \
                                           --untar_model_dir=${ROOT_DIR}/tmp \
                                           --output_file=${ROOT_DIR}/preli_wide_v3.csv \
                                           --input_data_pattern=${ROOT_DIR}/video/test*.tfrecord \
                                           --num_readers=16  

