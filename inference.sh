cd solution

ROOT_DIR=/home/apankratov/data/youtube8m_2018

CUDA_VISIBLE_DEVICES=1 python inference.py --train_dir ${ROOT_DIR}/models/prelu_wide  \
                                           --output_file=${ROOT_DIR}/preli_wide_v3.csv \
                                           --output_model_tgz=${ROOT_DIR}/prelu_wide_v3.tgz \
                                           --input_data_pattern=${ROOT_DIR}/video/test*.tfrecord \
                                           --num_readers=16  

